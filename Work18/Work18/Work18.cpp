﻿#include <iostream>

class Player
{
public:

    void SetInfo(std::string _name, int _points)
    {
        name = _name;
        points = _points;
    }

    void GetInfo()
    {
        std::cout << "Name: " << name << std::endl
            << "Points: " << points << std::endl;
    }

    bool operator > (Player& other)
    {
        return points > other.points;
    }

private:
    std::string name;
    int points;
};

int main() {

    int size = 0;

    std::cout << "Enter the size array : ";
    std::cin >> size;

    Player* player = new Player[size];


    for (int i = 0; i < size; ++i)
    {
        std::string name;
        int points;

        std::cout << "Enter the name " << i + 1 << " player: ";
        std::cin >> name;

        std::cout << "Enter the points " << i + 1 << " player: ";
        std::cin >> points;

        player[i].SetInfo(name, points);
    }

    int counter = size;

    while (counter--)
    {
        for (int j = 0; j < counter; ++j)
        {
            if (player[j] > player[j + 1])
            {
                std::swap(player[j], player[j + 1]);
            }
        }
    }

    std::cout << "===========================================" << std::endl;

    for (int i = size - 1; i >= 0; --i)
    {
        player[i].GetInfo();
        std::cout << "-------------------" << std::endl;
    }

    delete[] player;

    return 0;
}